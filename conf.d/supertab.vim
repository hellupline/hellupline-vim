inoremap <expr><cr> pumvisible() ? "<c-y>" : "<cr>"
let g:SuperTabDefaultCompletionType = '<c-x><c-o>'
