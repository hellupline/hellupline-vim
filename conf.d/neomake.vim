let g:neomake_verbose = 0

au! BufWritePost * Neomake
au! BufReadPost * Neomake

" C++1z support
let g:neomake_cpp_clang1z_maker = neomake#makers#ft#cpp#clang()
call add(g:neomake_cpp_clang1z_maker.args, '-pedantic')
call add(g:neomake_cpp_clang1z_maker.args, '-std=c++1z')
function! neomake#makers#ft#cpp#EnabledMakers()
    return ['clang1z']
endfunction
