" Use , as leader instead of \
let mapleader=','

" Get rid of search highlights
nmap <silent> <leader>h :noh<cr>
nmap <silent> <leader>H mhn`h

" Easier way to go toklast mark
nmap <silent> <leader>b <C-O>
" shortcut to saving from insert mode
inoremap :w <esc>:w
" correct regexp escaping
nnoremap / /\v
" easily reload vimrc
nnoremap <leader>R :source ~/.vim/vimrc<cr>


" Remove whitespace at EOL
au BufWritePre * :%s/\s\+$//e

" Space at EOL
" highlight WhitespaceEOL ctermbg=green
" match WhitespaceEOL /\s\+$/

" 80 collumn rule
" highlight OverLength ctermbg=red
" match OverLength /\%81v.\+/

" Current line
" highlight CursorLine cterm=none ctermbg=black
