set nocompatible " use vim defaults
set t_Co=256 " enable 256 color mode
set scrolloff=999 " center cursor vertically
set cursorline " highlight current line
set laststatus=2 " enable airline
set ttimeoutlen=0 " fix delay on airline on mode switching

set autoread " reread file when edited form outside
set showcmd " display incomplete commands
set wildmenu
set wildignore=*.o,*~,*.pyc,.git
set nobackup " do not keep a backup file
set noswapfile " do not need a swap file in 99.99%
set ruler " show the current row and column
set number " show line numbers
set nowrap " horizontal scrolling
set mouse=a " adds mouse support

set hlsearch " highlight searches
set incsearch " do incremental searching
set showmatch " jump to matches when entering regexp
set ignorecase " ignore case when searching
set smartcase " no ignorecase if Uppercase char present

set visualbell t_vb= " turn off error beep/flash
set novisualbell " turn off visual bell

set expandtab " Turn tabs into spaces
set shiftwidth=4 " Make indentation width 4 spaces
set softtabstop=4 " Make tab indent 4 spaces
set autoindent " Put the cursor at the correct indentation level
" set textwidth=72 " Wrap lines at column 72, more readable than 80
set formatoptions=cq " Disable automatic wrapping as you type

set conceallevel=2 " show concealing replacements
set concealcursor=n " do not show original text in normal mode

set completeopt=menu,menuone,longest " Disable doc window on completion
set backspace=indent,eol,start " make that backspace key work the way it should
set clipboard=unnamedplus " make system clipboard work with X
set omnifunc=syntaxcomplete#Complete

let $NVIM_TUI_ENABLE_TRUE_COLOR=1
let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1

syntax on
colorscheme wombat256

source ~/.vim/plugins.vim
for file in split(glob('~/.vim/conf.d/*.vim'), '\n')
    exe 'source' file
endfor
