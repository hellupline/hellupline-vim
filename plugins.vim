call plug#begin('~/.vim/plugged')

" Plug 'scrooloose/Syntastic'
" Plug 'scrooloose/nerdcommenter'
" Plug 'scrooloose/nerdtree'
Plug 'Townk/vim-autoclose'
Plug 'benekastah/neomake'
Plug 'bling/vim-airline'
Plug 'scrooloose/Syntastic'
Plug 'terryma/vim-multiple-cursors'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'

" Autocomplete
Plug 'Shougo/neocomplete.vim'
Plug 'ervandew/supertab'

" Python
Plug 'davidhalter/jedi-vim'
Plug 'lepture/vim-jinja'
Plug 'michaeljsmith/vim-indent-object'

" Web
Plug 'marijnh/tern_for_vim'
Plug 'othree/html5.vim'
Plug 'pangloss/vim-javascript'

" EyeCandy
Plug 'flazz/vim-colorschemes'

call plug#end()
