setlocal nosmartindent
setlocal omnifunc=jedi#completions

nmap <leader>T :!clear; nosetests -v<CR>
nmap <leader>t :!clear; nosetests -v %<CR>

nmap <leader>C :!clear; nosetests --with-coverage --cover-erase --cover-package .<CR>
nmap <leader>c :!clear; nosetests --with-coverage --cover-erase --cover-package %<CR>

nmap <leader>p :!clear; python -i %<CR>
nmap <leader>P :!clear; ipython -i %<CR>

let g:syntastic_python_checkers = ['python', 'pylint', 'pep8']

" jedi populates the completion list backwards
let g:SuperTabDefaultCompletionType = '<c-n>'
