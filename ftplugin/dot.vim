function! DrawGraph(layout)
    let command = '
        \TEMP=$(mktemp)
        \ && ' . a:layout . ' -Tpng % -o $TEMP
        \ && eog $TEMP &'
    execute ':silent !'.command
endfunction


nmap <silent> <leader>p <leader>Pd
nmap <silent> <leader>Pd :call DrawGraph('dot')<CR>
nmap <silent> <leader>Pc :call DrawGraph('circo')<CR>
nmap <silent> <leader>Pn :call DrawGraph('neato')<CR>
nmap <silent> <leader>Pt :call DrawGraph('twopi')<CR>
nmap <silent> <leader>Pf :call DrawGraph('fdp')<CR>
nmap <silent> <leader>Ps :call DrawGraph('sfdp')<CR>
nmap <silent> <leader>Pp :call DrawGraph('patchwork')<CR>
