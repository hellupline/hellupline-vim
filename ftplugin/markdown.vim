nmap <silent> <leader>= yyp:s/./=/g<CR>:nohl<CR>
nmap <silent> <leader>- yyp:s/./-/g<CR>:nohl<CR>
imap <silent> <leader>= <ESC><leader>=
imap <silent> <leader>- <ESC><leader>-
nmap <silent> <leader>p :silent !TEMP=$(mktemp) && markdown % > $TEMP && chromium --app="file://$TEMP" &<CR>
