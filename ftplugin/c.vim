" Enable C99 in syntastic
let g:syntastic_c_compiler='clang'
let g:syntastic_c_compiler_options='-std=c99'
