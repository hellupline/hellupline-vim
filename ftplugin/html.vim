setlocal sw=2 sts=2
setlocal omnifunc=htmlcomplete#CompleteTags

let g:html_indent_inctags = "html,head,body,tbody,ul,li,p"

" autoclose tags
" inoremap > ><esc>mqa</<C-x><C-o><esc>`qli
" inoremap /> />
